//
//  AppDelegate.swift
//  RealmTest
//
//  Created by Михаил on 28.05.2023.
//

import SwiftUI
import RealmSwift

class AppDelegate: NSObject, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        let config = Realm.Configuration(schemaVersion: 1)
        Realm.Configuration.defaultConfiguration = config
        
        return true
    }
}
