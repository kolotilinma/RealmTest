//
//  AddCardViewModel.swift
//  RealmTest
//
//  Created by Михаил on 29.05.2023.
//

import Foundation
import RealmSwift

class AddCardViewModel: ObservableObject {
    
    @Published var number: String
    
    internal init(number: String = "") {
        self.number = number
    }
    
    func addCard(number: String) {
                
        do {
            let realm = try Realm()
            try realm.write {
                realm.add(Card(number: number))
                
                self.number = ""
            }
        } catch {
            fatalError("Failed to add card: \(error.localizedDescription)")
        }
    }
}

