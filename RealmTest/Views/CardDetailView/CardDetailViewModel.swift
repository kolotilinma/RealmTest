//
//  CardDetailViewModel.swift
//  RealmTest
//
//  Created by Михаил on 29.05.2023.
//

import SwiftUI
import RealmSwift

class CardDetailViewViewModel: ObservableObject {
    
    @Published var number: String
    
    let card: Card
    
    var isFavorite: Binding<Bool> {
        Binding<Bool> {
            self.card.isFavorite
        } set: { value in
            self.card.update(isFavorite: value)
        }
    }
    
    init(card: Card) {
        
        self.card = card
        self.number = card.number
        
    }
    
    func deleteCard(card: Card) {
        do {
            let realm = try Realm()
            try realm.write {
                
                realm.delete(card)
            }
        } catch {
            fatalError("Failed to delete card: \(error.localizedDescription)")
        }
    }
    
}
 
