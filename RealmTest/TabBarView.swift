//
//  TabBarView.swift
//  RealmTest
//
//  Created by Михаил on 28.05.2023.
//

import SwiftUI

struct TabBarView: View {
    
    @State private var selection = 0
    
    var body: some View {
        
        TabView(selection: $selection) {
            
            CardsListView(viewModel: .init())
                .tabItem {
                    Image(systemName: "creditcard")
                    Text("Cards")
                }
                .tag(0)
            
            AddCardView(viewModel: .init())
                .tabItem {
                    Image(systemName: "plus.circle.fill")
                    Text("Add Card")
                }
                .tag(1)
        }
    }
}

struct TabBarView_Previews: PreviewProvider {
    static var previews: some View {
        TabBarView()
    }
}
