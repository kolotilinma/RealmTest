//
//  CardsListView.swift
//  RealmTest
//
//  Created by Михаил on 28.05.2023.
//

import SwiftUI

struct CardsListView: View {
    
    @ObservedObject var viewModel: CardsListViewModel
    
    var body: some View {
        
        NavigationView {
            
            List {
                
                ForEach(viewModel.cards) { card in
                    Button(action: {
                        
                        viewModel.openCardDetail(card: card)
                    
                    }) {
                        HStack {
                            
                            Text(card.number)
                            
                            Spacer()
                            
                            if card.isFavorite {
                                Image(systemName: "heart.fill")
                                    .foregroundColor(.pink)
                            }
                        }
                    }
                }
                .onDelete { indices in
                    let cardsToDelete = indices.map { viewModel.cards[$0] }
                    cardsToDelete.forEach { card in
                        viewModel.deleteCard(card: card)
                    }
                }
            }
            .navigationTitle("Credit Cards")
            .sheet(item: $viewModel.sheet) { item in
                
                switch item.type {
                    
                case .cardDetail(let cardDetail):
                    CardDetailView(viewModel: cardDetail)
                    
                }
            }
        }
    }
}

struct CardsListView_Previews: PreviewProvider {
    static var previews: some View {
        CardsListView(viewModel: CardsListViewModel())
    }
}
