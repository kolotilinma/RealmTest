//
//  CardsListViewModel.swift
//  RealmTest
//
//  Created by Михаил on 29.05.2023.
//

import SwiftUI
import Combine
import RealmSwift

class CardsListViewModel: ObservableObject {
    
    @Published var sheet: Sheet?
    
    @Published var cards: [Card] = []
        
    private var cancellable = Set<AnyCancellable>()
    
    init() {
        
        bind()
    }
    
    private func bind() {
        
        do {
            let realm = try Realm()
            realm.objects(Card.self)
                .collectionPublisher
                .tryMap { Array($0) }
                .receive(on: DispatchQueue.main)
                .sink { [unowned self] completion in
                    switch completion {
                        
                    case .finished:
                        break
                        
                    case .failure(let error):
                        print("Ошибка при получении данных из Realm: \(error)")
                    }
                } receiveValue: { [unowned self] cards in
                    self.cards = cards
                }
                .store(in: &cancellable)
        } catch {
            // Обработка ошибки создания объекта Realm
            print("Ошибка создания объекта Realm: \(error)")
        }
        
    }
    
    func openCardDetail(card: Card) {
        
        let cardDetailViewModel = CardDetailViewViewModel(card: card)
        sheet = Sheet(type: .cardDetail(cardDetailViewModel))
    }
    
    func deleteCard(card: Card) {
        do {
            let realm = try Realm()
            
            try realm.write {
                realm.delete(card)
            }
        } catch {
            fatalError("Failed to delete card: \(error.localizedDescription)")
        }
    }

    struct Sheet: Identifiable, Equatable {
        
        static func == (lhs: CardsListViewModel.Sheet, rhs: CardsListViewModel.Sheet) -> Bool {
            lhs.id == rhs.id
        }
        
        let id = UUID()
        let type: Kind
        
        enum Kind {
            
            case cardDetail(CardDetailViewViewModel)
        }
    }
    
}
