//
//  RealmTestApp.swift
//  RealmTest
//
//  Created by Михаил on 28.05.2023.
//

import SwiftUI

@main
struct RealmTestApp: App {
    
    @UIApplicationDelegateAdaptor(AppDelegate.self) var delegate
    
    var body: some Scene {
        WindowGroup {
            TabBarView()
        }
    }
}
