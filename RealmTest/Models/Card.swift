//
//  Card.swift
//  RealmTest
//
//  Created by Михаил on 28.05.2023.
//

import Foundation
import RealmSwift

class Card: Object, ObjectKeyIdentifiable {
    
    @Persisted(primaryKey: true) var _id: ObjectId
    @Persisted var number = ""
    @Persisted var isFavorite = false
    
    convenience init(number: String) {
        
        self.init()
        self.number = number
    }
    
    func update(number: String) {
        if let realm = self.realm {
            try? realm.write({
                self.number = number
            })
        }else {
            self.number = number
        }
    }
    
    func update(isFavorite: Bool) {
        if let realm = self.realm {
            try? realm.write({
                self.isFavorite = isFavorite
            })
        }else {
            self.isFavorite = isFavorite
        }
    }
    
}
