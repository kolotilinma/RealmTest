//
//  CardDetailView.swift
//  RealmTest
//
//  Created by Михаил on 28.05.2023.
//

import SwiftUI

struct CardDetailView: View {
    
    @ObservedObject var viewModel: CardDetailViewViewModel
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        VStack(spacing: 24) {
            
            Text("Card Details")
                .font(.largeTitle)
                .padding()
            
            TextField("edit", text: $viewModel.number)
                .textFieldStyle(RoundedBorderTextFieldStyle())
            
            Toggle(isOn: viewModel.isFavorite, label: {
                Text("select as favorite")
            })
            
            Button(action: {
                
                viewModel.card.update(number: viewModel.number)
//                presentationMode.wrappedValue.dismiss()
            }, label: {
                Text("save")
            })
            .buttonStyle(.borderedProminent)
            
            Button(action: {
                viewModel.deleteCard(card: viewModel.card)
                presentationMode.wrappedValue.dismiss()
            
            }) {
                Text("Delete Card")
                    .foregroundColor(.red)
                    .padding(.top, 20)
            }
            
            Spacer()
        }
        .padding(.horizontal, 16)
        .navigationBarTitle(Text("Card Details"), displayMode: .inline)
    }
}


struct CardDetailView_Previews: PreviewProvider {
    static var previews: some View {
        
        let card = Card(number: "1234 5678 9012 3456")
        let viewModel = CardDetailViewViewModel(card: card)
        
        return CardDetailView(viewModel: viewModel)

    }
}
