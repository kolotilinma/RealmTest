//
//  AddCardView.swift
//  RealmTest
//
//  Created by Михаил on 28.05.2023.
//

import SwiftUI

struct AddCardView: View {
    
    @ObservedObject var viewModel: AddCardViewModel
        
    var body: some View {
        
        VStack {
            
            TextField("Card Number", text: $viewModel.number)
                .textFieldStyle(RoundedBorderTextFieldStyle())
                .padding()
            
            Button(action: {
                
                viewModel.addCard(number: viewModel.number)
                
            }) {
                
                Text("Add Card")
                    .padding()
                    .background(Color.blue)
                    .foregroundColor(.white)
                    .cornerRadius(10)
            }
            .padding()
        }
        .navigationTitle("Add Card")
    }
}
struct AddCardView_Previews: PreviewProvider {
    
    static var previews: some View {
        AddCardView(viewModel: .init())
    }
}
